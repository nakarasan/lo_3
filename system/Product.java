package system;

public class Product {
	
		private int id;
		private String name;
		private float price;
		//quantity
		private int qu;

		public Product() {
			super();
		}

		@Override
		public String toString() {
			return "Product [id=" + id + ", name=" + name + ", price=" + price + ", num=" + qu + "]";
		}
		
		public Product(int id, String i, float price, int qu) {
			super();
			this.id = id;
			this.name = i;
			this.price = price;
			this.qu = qu;
		}

		// set get method
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getName() {
			return 0;
		}
		
		public void setName(String name) {
			this.name = name;
		}

		public float getPrice() {
			return price;
		}

		public void setPrice(float price) {
			this.price = price;
		}

		public int getNum() {
			return qu;
		}

		public void setNum(int qu) {
			this.qu = qu;
		}

		

}
