package system;

import java.sql.PreparedStatement;

public class Main {
	
	public static void main(String[] args, PreparedStatement s) {
		
		Function function = new Function();
		
		System.out.println("Welcome to our Online Shop");
		function.nUse = "nextUse";
		
	do {
		
		function.showMenu();
		System.out.println("Please enter the action to be performed");
		String select = function.sc.next();
		
		switch (select) {
		case "1":
			function.insertProduct();
			break;
			
		case "2":
			System.out.println("Enter the item number to Search");
			int goodId = function.sc.nextInt();
			Product good = function.searchProduct(goodId);
			
			if (good != null) {
				System.out.println("Product Id: "+goodId + " Product Name: " + good.getName()
						+ "  Price:" + good.getPrice() + "  Quantity: " + good.getNum());
			} else {
				System.out.println("This product doen't exist");
			}
			break;

		case "3":
			function.getProductList(s);
			break;

		case "4":
			function.buyProduct();
			break;
		
		case "5":
			System.out.println("Enter the number of item to delete");
			int id = function.sc.nextInt();
			function.deleteProduct(id);
			
			break;
			
		case "6":
			function.updateGood();
			break;
		
		case "0":
			System.out.println("Thank You");
			System.exit(0);
		default:
			System.err.println("Please, enter the correct number");
			continue;
		}
		System.out.println("input nextUse Continue, Otherwise quit system");
		function.nUse = function.sc.next();
	} while (function.nUse.equals("nextUse"));
	System.out.println("Welcome for the next use");
}

}
