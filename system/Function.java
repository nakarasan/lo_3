package system;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class Function {
	public Scanner sc = new Scanner(System.in);
	public String nUse = "nextUse";
	
	
	public void insertProduct() {

		int id = 0;
		System.out.println("Enter item number");
		while (true) {
			id = sc.nextInt();
			
			if (searchProduct(id) == null) {
				break;
			}
			System.err.println("Enter the correct number of product");
		}

		System.out.println("Enter product name");
		String name = sc.next();
		System.out.println("Enter unit price of goods");
		float price = sc.nextFloat();
		System.out.println("Enter item quantity");
		int qu = sc.nextInt();

		String sql = "insert into proTbl()values(?,?,?,?)";
		try {

			Connection con = JDBC_Con.getConnection();

			java.sql.PreparedStatement pst = con.prepareStatement(sql);

			pst.setInt(1, id);
			pst.setString(2, name);
			pst.setFloat(3, price);
			pst.setInt(4, qu);

			if (!pst.execute()) {
				System.out.println("Warehousing success");
			}

			JDBC_Con.close(con, pst);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Abnormal storage" + e.getMessage());
		}
	}
	
	
	public void getProductList(PreparedStatement s) {

		String sql = "select id,name,price,qu from proTbl";
		try {
			Connection con = JDBC_Con.getConnection();

			PreparedStatement pst = s;
			ResultSet rs = pst.executeQuery();
			System.out.println("number\t" + "Name\t" + "Unit Price\t" + "Number\t");
			if (rs.wasNull()) {
				System.out.println("No products");
			} else {
				while (rs.next()) {
					
					System.out.println(rs.getInt("id") + "\t" + rs.getString("name") + "\t" + rs.getFloat("price")
							+ "\t" + rs.getInt("num") + "\t");
				}
			}
			
			JDBC_Con.close(con, pst);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public void buyProduct() {

		ArrayList<Product> products = new ArrayList<>();

		String flag = "nextUse";
		
		do {
			System.out.println("Enter number of purchase item");
			int id = sc.nextInt();
			
			Product good = searchProduct(id);
			
			if (good != null) {
				System.out.println("Enter the purchased products Quantity");
				
				int num = sc.nextInt();
				if (good.getNum() < num) {
					System.out.println("This product is not in the Stoke, Sorry...");

				} else {
					try {
						String sql = "update proTbl set qu=? where id=?";
						
						Connection con = JDBC_Con.getConnection();
						
						java.sql.PreparedStatement pst = con.prepareStatement(sql);
						pst.setInt(1, good.getNum() - num);// Update stock
						pst.setInt(2, id);
						if (pst.executeUpdate() == 1) {
							
							Product p = new Product();
							if (products.size() > 0) {
								for (int i = 0; i < products.size(); i++) {
									if (products.get(i).getId() == id) {
										
										products.get(0).setNum(num + products.get(0).getNum());
									} else {
										products.add(p);
									}
								}
							} else {
								products.add(p);
							}
							System.out.println("Purchase success!!!");
						} else {
							System.out.println("Purchase failure!");
						}
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println("Purchase exception" + e.getMessage());
					}
				}
				System.out.println("input y Continue to buy/Enter other settlement");
				flag = sc.next();
				if (!flag.equals("nextUse")) {
					
					accountDetails(products);
				}
			} else {
				System.out.println("There is no such product");
			}
		} while (flag.equals("nextUse"));
	}


	public void accountDetails(ArrayList<Product> products) {
		System.out.println("number\t" + "Name\t" + "Number\t" + "Total price");

		products.forEach(in -> System.out.println(in.getId() + "\t" + in.getName() + "\t" + in.getId() + "\t" + in.getNum() * in.getPrice()));
		
		float totPrice = 0;
		for (int i = 0; i < products.size(); i++) {
			
			totPrice += (products.get(i).getNum()) * (products.get(i).getPrice());
		}
		System.out.println("Aggregate consumption:" + totPrice + "element");
	}
	
	
	public void deleteProduct(int id) {
		String sql = "delete from proTbl where id=?";
		try {
			
			Connection con = JDBC_Con.getConnection();			
			PreparedStatement pst = (PreparedStatement) con.prepareStatement(sql);			
			pst.setInt(1, id);
			
			if (!pst.execute()) {
				System.out.println("Delete successful!!!");
			}
			JDBC_Con.close(con, pst);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Delete exception" + e.getMessage());
		}
	}
	
	public void updateGood() {
		System.out.println("Enter the item id to update");
		int gid = sc.nextInt();
		Product good = searchProduct(gid);
		System.out.println("Product information");
		if (good != null) {
			System.out.println("Commodity number:" + gid + " Trade name:" + good.getName() + "  commodity price:"
					+ good.getPrice() + "  Quantity of commodities:" + good.getNum());
			System.out.println("Modify product name");
			String name = sc.next();
			System.out.println("Modify commodity unit price");
			float price = sc.nextFloat();
			System.out.println("Modify product Quantity");
			int num = sc.nextInt();
			String sql = "update proTbl set name=?,price=?,qu=? where id=? ";
			try {
				
				Connection con = JDBC_Con.getConnection();
				PreparedStatement pst = (PreparedStatement) con.prepareStatement(sql);
				
				pst.setString(1, name);
				pst.setFloat(2, price);
				pst.setInt(3, num);
				pst.setInt(4, gid);
				
				if (!pst.execute()) {
					System.out.println("Update success");
				}
				
				JDBC_Con.close(con, pst);
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Update exception" + e.getMessage());
			}
		} else {
			System.out.println("This product does not exist");
		}
	}
	
	public Product searchProduct(int id) {
		
		String sql = "select id,name,price,qu from proTbl where id=?";
		try {
			Connection con = JDBC_Con.getConnection();
			java.sql.PreparedStatement pst = con.prepareStatement(sql);
			
			pst.setInt(1, id);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {
				Product good = new Product(rs.getInt("id"), rs.getString("name"), rs.getFloat("price"), rs.getInt("num"));
				return good;
			}
	
			JDBC_Con.close(con, pst);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void showMenu() {
		System.out.println("1.Prouducts warehousing");
		System.out.println("2.Query products according to product Id");
		System.out.println("3.List of commodities");
		System.out.println("4.Purchase");
		System.out.println("5.Delete Product");
		System.out.println("6.Update Product");
		System.out.println("0.Exit");
	}

}
